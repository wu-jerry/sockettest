package com.rj.httpserver;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class HttpServer {
    private ServerSocket serverSocket = null;
    private Map<String, String> parameters = new HashMap<>();

    public HttpServer(int port) throws IOException {
        serverSocket = new ServerSocket(port);
    }

    public void start() throws IOException {
        System.out.println("服务器启动");
        ExecutorService executorService = Executors.newCachedThreadPool();
        while (true) {
            // 1. 获取连接
            Socket clientSocket = serverSocket.accept();
            // 2. 处理连接(使用短连接的方式实现)
            executorService.execute(new Runnable() {
                @Override
                public void run() {
                    process(clientSocket);
                }
            });
        }
    }

    private void process(Socket clientSocket) {
        // 由于 HTTP 协议是文本协议, 所以仍然使用字符流来处理.
        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
             BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()))) {
            // 1. 读取请求并解析
            //  a) 解析首行, 三个部分使用空格切分
            String firstLine = bufferedReader.readLine();
            String[] firstLineTokens = firstLine.split(" ");
            String method = firstLineTokens[0];
            String url = firstLineTokens[1];
            String version = firstLineTokens[2];
            //  b). 解析 url 中的参数
            int pos = url.indexOf("?");
            if (pos != -1) {
                String parameter = url.substring(pos + 1);
                parseKV(parameter, parameters);
            }
            //  c) 解析 header, 按行读取, 然后按照冒号空格来分割键值对
            Map<String, String> headers = new HashMap<>();
            String line = "";
            while ((line = bufferedReader.readLine()) != null && line.length() != 0) {
                String[] headerTokens = line.split(": ");
                headers.put(headerTokens[0], headerTokens[1]);
            }
            System.out.printf("%s %s %s\n", method, url, version);
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                System.out.println(entry.getKey() + ": " + entry.getValue());
            }
            System.out.println();
            // 2. 根据请求计算响应
            String resp = "";
            if (url.startsWith("/add")) {
                int a = Integer.parseInt(parameters.get("a"));
                int b = Integer.parseInt(parameters.get("b"));
                int result = a + b;
                bufferedWriter.write(version + " 200 OK\n");
                resp = "<h1>a + b = " + result + "</h1>";
            } else if (url.startsWith("/mul")) {
                int a = Integer.parseInt(parameters.get("a"));
                int b = Integer.parseInt(parameters.get("b"));
                int result = a * b;
                bufferedWriter.write(version + " 200 OK\n");
                resp = "<h1>a * b = " + result + "</h1>";
            } else if (url.startsWith("/reduce")) {
                int a = Integer.parseInt(parameters.get("a"));
                int b = Integer.parseInt(parameters.get("b"));
                int result = a - b;
                bufferedWriter.write(version + " 200 OK\n");
                resp = "<h1>a - b = " + result + "</h1>";
            } else if (url.startsWith("/divide")) {
                float a = Integer.parseInt(parameters.get("a"));
                float b = Integer.parseInt(parameters.get("b"));
                float result = a / b;
                bufferedWriter.write(version + " 200 OK\n");
                resp = "<h1>a - b = " + result + "</h1>";
            }
            // 3. 把响应写回到客户端
            bufferedWriter.write("Content-Type: text/html\n");
            bufferedWriter.write("Content-Length: " + resp.getBytes().length + "\n"); // 此处的长度, 不能写成 resp.length(), 得到的是字符的数目, 而不是字节的数目
            bufferedWriter.write("\n");
            bufferedWriter.write(resp);

            bufferedWriter.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                clientSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static void parseKV(String input, Map<String, String> output) {
        // 1. 先按照 & 切分成若干组键值对
        String[] kvTokens = input.split("&");
        // 2. 针对切分结果再分别进行按照 = 切分, 就得到了键和值
        for (String kv : kvTokens) {
            String[] result = kv.split("=");
            output.put(result[0], result[1]);
        }
    }

    public static void main(String[] args) throws IOException {
        HttpServer server = new HttpServer(9090);
        server.start();
    }
}

